package nl.producerstore.productservice.web.dto;

public interface DataTransferObject<T> {
    T toModel();
}
