package nl.producerstore.productservice.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import nl.producerstore.productservice.app.CreationException;
import nl.producerstore.productservice.app.DeletionException;
import nl.producerstore.productservice.app.UpdateException;
import nl.producerstore.productservice.service.ProductService;
import nl.producerstore.productservice.web.dto.ProductDto;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.net.URI;

@Path("/products")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class ProductController {

    @Inject
    private ProductService service;

    @GET
    @Operation(description = "Get a list of all products", tags = {"Products"} ,responses = {
            @ApiResponse(responseCode = "200", description = "Returns a list of all products"),
            @ApiResponse(responseCode = "500", description = "Something went wrong when retrieving the products")
    })
    public Response getAll() {
        try {
            return Response.ok()
                    .entity(service.getAll())
                    .build();
        } catch (Exception e) {
            return internalServerError(e.getMessage());
        }
    }

    @GET
    @Path("/{id}")
    @Operation(description = "Get a specific product using its ID.", tags = {"Products"},
            parameters = {@Parameter(name = "id", description = "The ID associated with the product.", schema = @Schema(implementation = Long.class))},
            responses = {
                    @ApiResponse(responseCode = "200", description = "The product is returned to the client"),
                    @ApiResponse(responseCode = "404", description = "No product with the given ID was found.")
            }
    )
    public Response getById(@PathParam("id") long id, @Context UriInfo uriInfo) {
        try {
            return Response.ok()
                    .entity(service.getById(id))
                    .link(uriInfo.getAbsolutePathBuilder().build(id), "self")
                    .build();
        } catch (NotFoundException e) {
            return notFound(id);
        }
    }

    @POST
    @Operation(description = "Create a new product.",  tags = {"Products"},
            requestBody = @RequestBody(description = "The product to be created.", required = true, content = @Content(schema = @Schema(implementation = ProductDto.class))),
            responses = {
                    @ApiResponse(responseCode = "201", description = "The product was created successfully", headers = {@Header(name = "Location", description = "The location at which the product was created.")}),
                    @ApiResponse(responseCode = "500", description = "Something went wrong during the creation of the product.")
            }
    )
    public Response createProduct(/*@Valid */ProductDto productDto, @Context UriInfo uriInfo) {
        try {
            long createdId = service.add(productDto.toModel());

            URI uri = uriInfo.getAbsolutePathBuilder().segment(Long.toString(createdId)).build();

            return Response.created(uri)
                    .link(uriInfo.getAbsolutePathBuilder().build(), "self")
                    .build();
        } catch (CreationException e) {
            return internalServerError(e.getMessage());
        }
    }

    @DELETE
    @Path("/{id}")
    @Operation(description = "Delete a specific product.", tags = {"Products"},
            parameters = {@Parameter(name = "id", description = "The ID associated with the product.")},
            responses = {
                    @ApiResponse(responseCode = "201", description = "The product was deleted successfully."),
                    @ApiResponse(responseCode = "404", description = "No product was found with the given ID."),
                    @ApiResponse(responseCode = "500", description = "Something went wrong during the deletion of the product.")
            }
    )
    public Response deleteProduct(@PathParam("id") long id) {
        try {
            service.delete(id);
            return Response.status(204)
                    .build();

        } catch (NotFoundException e) {
            return notFound(id);
        } catch (DeletionException e) {
            return internalServerError(e.getMessage());
        }
    }

    @PUT
    @Path("/{id}")
    @Operation(description = "Update a specific product.", tags = {"Products"},
            parameters = {
                    @Parameter(name = "id", description = "The ID associated with the product.", schema = @Schema(implementation = Long.class)),
                    @Parameter(name = "new values", description = "The values to be updated.", schema = @Schema(implementation = ProductDto.class))
            },
            responses = {
                    @ApiResponse(responseCode = "204", description = "The product was updated successfully."),
                    @ApiResponse(responseCode = "404", description = "No product was found with the given ID."),
                    @ApiResponse(responseCode = "500", description = "Something went wrong during the update of the product.")
            }
    )
    public Response updateProduct(@PathParam("id") long id, ProductDto newValues) {
        try {
            service.update(id, newValues.toModel());
            return Response.status(204)
                    .build();

        } catch (NotFoundException e) {
            return notFound(id);
        } catch (UpdateException e) {
            return internalServerError(e.getMessage());
        }
    }

    // Helper methods
    @HEAD
    @Operation(tags = {"Health checks"})
    public Response head() {
        return Response.ok().header("X-Healthy", true).build();
    }

    private Response notFound(long id) {
        return Response.status(404)
                .entity("Product with id \"" + id + "\" does not exist.")
                .type(MediaType.TEXT_PLAIN_TYPE)
                .build();
    }

    private Response internalServerError(String message) {
        return Response.status(500).type(MediaType.TEXT_PLAIN_TYPE).entity(message).build();
    }
}
