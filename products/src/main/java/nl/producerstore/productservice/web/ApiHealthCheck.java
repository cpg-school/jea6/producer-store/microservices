package nl.producerstore.productservice.web;

import org.eclipse.microprofile.health.Health;
import org.eclipse.microprofile.health.HealthCheck;
import org.eclipse.microprofile.health.HealthCheckResponse;
import org.eclipse.microprofile.health.HealthCheckResponseBuilder;

import javax.enterprise.context.ApplicationScoped;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

@Health
@ApplicationScoped
public class ApiHealthCheck implements HealthCheck {

    private static final String url = "http://localhost:8080/products";
    private static final Logger logger = Logger.getLogger(ApiHealthCheck.class.getName());

    @Override
    public HealthCheckResponse call() {
        HealthCheckResponseBuilder response = HealthCheckResponse.named("Endpoint: /products");

        try {
            HttpURLConnection connection = (HttpURLConnection) new URL(url).openConnection();
            connection.setRequestMethod("HEAD");

            if (connection.getResponseCode() == 200) {
                return response.up().build();
            }
        } catch (Exception e) {
            logger.severe(e.getMessage());
        }

        return response.down().build();
    }
}
