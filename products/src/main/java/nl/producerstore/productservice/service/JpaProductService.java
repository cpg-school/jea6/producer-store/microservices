package nl.producerstore.productservice.service;

import nl.producerstore.productservice.app.CreationException;
import nl.producerstore.productservice.app.DeletionException;
import nl.producerstore.productservice.app.UpdateException;
import nl.producerstore.productservice.model.Product;

import javax.annotation.Priority;
import javax.enterprise.context.ApplicationScoped;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import javax.ws.rs.NotFoundException;
import java.util.List;

@ApplicationScoped
@Priority(1)
public class JpaProductService implements ProductService {

    @PersistenceContext(unitName = "product-service")
    private EntityManager manager;

    @Override
    public List<Product> getAll() {
        return manager.createQuery("SELECT m FROM Product m", Product.class).getResultList();
    }

    @Override
    public Product getById(long id) throws NotFoundException {
        Product product = manager.find(Product.class, id);

        if (product == null) {
            throw new NotFoundException();
        }

        return product;
    }

    @Override
    @Transactional
    public long add(Product product) throws CreationException {
        try {
            manager.persist(product);
            manager.flush();

            return product.getId();
        } catch (Exception e) {
            throw new CreationException("Could not create product.\n".concat(e.getMessage()));
        }
    }

    @Override
    @Transactional
    public void delete(long id) throws DeletionException {
         Product product = manager.find(Product.class, id);

        if (product == null) {
            throw new NotFoundException();
        }

        try {
            manager.remove(manager.contains(product) ? product : manager.merge(product));
        } catch (Exception e) {
            throw new DeletionException(e);
        }
    }

    @Override
    @Transactional
    public void update(long id, Product newValues) throws UpdateException {
        Product original = manager.find(Product.class, id);

        if (original == null) {
            throw new NotFoundException();
        }

        try {
            newValues.setId(id);
            manager.merge(newValues);
        } catch (Exception e) {
            throw new UpdateException(e);
        }
    }
}
