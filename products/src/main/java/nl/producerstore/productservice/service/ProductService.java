package nl.producerstore.productservice.service;

import nl.producerstore.productservice.app.CreationException;
import nl.producerstore.productservice.app.DeletionException;
import nl.producerstore.productservice.app.UpdateException;
import nl.producerstore.productservice.model.Product;

import javax.ws.rs.NotFoundException;
import java.util.List;

public interface ProductService {
    /**
     * Get a list of all products.
     * @return A list of all products.
     */
    List<Product> getAll();

    /**
     * Get a specific product by ID.
     * @param id The ID associated with the product.
     * @return The specified product.
     * @throws NotFoundException No product with this ID was found.
     */
    Product getById(long id) throws NotFoundException;

    /**
     * Add a new product.
     * @param product The product to be added.
     * @return The ID associated with the newly created product.
     * @throws CreationException Something went wrong during the creation of the product.
     */
    long add(Product product) throws CreationException;

    /**
     * Remove a specific product.
     * @param id The ID associated with the product.
     * @throws DeletionException Something went wrong during the removal of the product.
     */
    void delete(long id) throws DeletionException;

    /**
     * Update a specific product.
     * @param id The ID associated with the product.
     * @param newValues The updated values to save.
     * @throws UpdateException Something went wrong during the update of the product.
     */
    void update(long id, Product newValues) throws UpdateException;
}
