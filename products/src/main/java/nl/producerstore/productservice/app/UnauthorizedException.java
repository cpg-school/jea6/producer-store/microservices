package nl.producerstore.productservice.app;

public class UnauthorizedException extends Exception {

    public UnauthorizedException() {

    }

    public UnauthorizedException(String s) {
        super(s);
    }
}
