package nl.producerstore.productservice.app;

public class DeletionException extends Exception {

    public DeletionException() {

    }

    public DeletionException(Throwable cause) {
        initCause(cause);
    }

    @Override
    public String getMessage() {
        return "Could not perform delete."
                .concat(this.getCause() == null
                        ? ""
                        : "With cause: ".concat(this.getCause().getLocalizedMessage()));
    }
}
