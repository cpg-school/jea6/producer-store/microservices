package nl.producerstore.productservice.app;

public class CreationException extends Exception {
    public CreationException(String errorMessage) {
        super(errorMessage);
    }
}
