package nl.producerstore.productservice.app;

public class UpdateException extends Exception {
    public UpdateException(String message) {
        super(message);
    }

    public UpdateException(Exception cause) {
        initCause(cause);
    }

    @Override
    public String getMessage() {
        return "Could not perform update."
                .concat(this.getCause() == null
                        ? ""
                        : "With cause: ".concat(this.getCause().getLocalizedMessage()));
    }
}
