package tests;

import nl.producerstore.productservice.app.CreationException;
import nl.producerstore.productservice.app.DeletionException;
import nl.producerstore.productservice.app.UpdateException;
import nl.producerstore.productservice.model.Product;
import nl.producerstore.productservice.service.ProductService;
import nl.producerstore.productservice.web.controller.ProductController;
import nl.producerstore.productservice.web.dto.ProductDto;
import org.glassfish.hk2.utilities.binding.AbstractBinder;
import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.test.JerseyTest;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;
import static utillities.ProductTestUtillities.createProduct;
import static utillities.ProductTestUtillities.createProductDto;

public class ProductControllerTests {

    private static ProductService mockService = mock(ProductService.class);
    private static JerseyTest jersey;

    //region setup region
    @BeforeClass
    public static void setup() throws Exception {
        jersey = new JerseyTest() {
            @Override
            protected Application configure() {
                return new ResourceConfig(ProductController.class)
                        .register(new AbstractBinder() {
                            @Override
                            protected void configure() {
                                bind(mockService).to(ProductService.class);
                            }
                        });
            }
        };

        jersey.setUp();
    }

    @AfterClass
    public static void teardown() throws Exception {
        jersey.tearDown();
    }

    @Before
    public void beforeEach() {
        reset(mockService);
    }
    //endregion

    @Test
    public void getAll_ReturnsOk_WithListOfProjects() {
        when(mockService.getAll()).thenReturn(Arrays.asList(createProduct(), createProduct(), createProduct()));

        Response response = jersey.target("/products").request().get();

        assertEquals(200, response.getStatus());
        assertEquals(Product[].class, response.readEntity(Product[].class).getClass());
    }

    @Test
    public void gtById_ReturnsOk_WithCorrectProject() {
        long id = 1;
        Product product = new Product();
        product.setId(id);

        when(mockService.getById(id)).thenReturn(product);

        Response response = jersey.target("/products/".concat(id + "")).request().get();

        assertEquals(200, response.getStatus());
        verify(mockService).getById(id);
    }

    @Test
    public void getById_NoProductWithGivenId_Returns404() {
        long id = 1;
        String errorMessage = "Product with id \""
                .concat(Long.toString(id))
                .concat("\" does not exist.");

        when(mockService.getById(id)).thenThrow(new NotFoundException(errorMessage));

        Response response = jersey.target("/products/".concat(Long.toString(id))).request().get();

        assertEquals(404, response.getStatus());
        assertEquals(errorMessage, response.readEntity(String.class));
    }

    @Test
    public void createProduct_CorrectlyFormattedObject_Returns201() throws CreationException {
        ProductDto product = createProductDto();
        Entity productEntity = Entity.entity(product, MediaType.APPLICATION_JSON);
        String expectedLocation = jersey.target().getUri().toString().concat("products/").concat("1");

        when(mockService.add(any(Product.class))).thenReturn(1L);

        Response response = jersey.target("/products").request().post(productEntity);

        assertEquals(201, response.getStatus());
        assertEquals(expectedLocation, response.getHeaderString("location"));
    }

    @Test
    public void createProduct_WronglyFormattedObject_Returns400() {
        Entity entity = Entity.entity("NOT_A_PRODUCT", MediaType.APPLICATION_JSON_TYPE);

        Response response = jersey.target("/products").request().post(entity);

        assertEquals(400, response.getStatus());
    }

    @Test
    public void createProduct_ThrowsException_Returns500() throws CreationException {
        String errorMessage = "Something went wrong...";

        when(mockService.add(any(Product.class))).thenThrow(new CreationException(errorMessage));

        Response response = jersey.target("/products")
                .request()
                .post(Entity.entity(createProductDto(), MediaType.APPLICATION_JSON_TYPE)
                );

        assertEquals(500, response.getStatus());
        assertEquals(errorMessage, response.readEntity(String.class));
    }

    @Test
    public void deleteProduct_ProductExists_Returns204() {
        when(mockService.getById(1)).thenReturn(createProduct());

        Response response = jersey.target("/products/".concat(Long.toString(1)))
                .request()
                .delete();

        assertEquals(204, response.getStatus());
    }

    @Test
    public void deleteProduct_ProductDoesNotExist_Returns404() throws DeletionException {
        long id = 1;

        doThrow(new NotFoundException()).when(mockService).delete(id);

        Response response = jersey.target("/products/".concat(Long.toString(id)))
                .request()
                .delete();

        assertEquals(404, response.getStatus());
    }

    @Test
    public void deleteProduct_ProductExists_CannotBeDeleted_Returns500() throws DeletionException {
        long id = 1;

        doThrow(new DeletionException()).when(mockService).delete(id);

        Response response = jersey.target("/products/".concat(Long.toString(id)))
                .request()
                .delete();

        assertEquals(500, response.getStatus());
    }

    @Test
    public void updateProduct_CorrectlyFormattedObject_Returns204() throws UpdateException {
        long id = 1;

        doNothing().when(mockService).update(any(Long.class), any(Product.class));

        Response response = jersey.target("/products/".concat(Long.toString(id)))
                .request()
                .put(Entity.entity(new ProductDto(), MediaType.APPLICATION_JSON_TYPE));

        assertEquals(204, response.getStatus());
    }

    @Test
    public void updateProduct_ProductNotFound_Returns404() throws UpdateException {
        long id = 1;

        doThrow(new NotFoundException()).when(mockService).update(any(Long.class), any(Product.class));

        Response response = jersey.target("/products/".concat(Long.toString(id)))
                .request()
                .put(Entity.entity(new ProductDto(), MediaType.APPLICATION_JSON_TYPE));

        assertEquals(404, response.getStatus());
    }

    @Test
    public void updateProduct_InternalError_Returns500() throws UpdateException {
        long id = 1;

        doThrow(new UpdateException("")).when(mockService).update(any(Long.class), any(Product.class));

        Response response = jersey.target("/products/".concat(Long.toString(id)))
                .request()
                .put(Entity.entity(new ProductDto(), MediaType.APPLICATION_JSON_TYPE));

        assertEquals(500, response.getStatus());
    }
}
