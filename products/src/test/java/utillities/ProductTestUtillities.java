package utillities;

import nl.producerstore.productservice.model.Product;
import nl.producerstore.productservice.web.dto.ProductDto;

import java.util.Arrays;

public class ProductTestUtillities {
    public static Product createProduct() {
        Product product = new Product(stringOfLength(10), stringOfLength(25), 9.99, "http://source.com/image.png");
        product.setId(1);

        return product;
    }

    public static ProductDto createProductDto() {
        Product p = createProduct();
        ProductDto dto = new ProductDto();

        dto.setImageUrl(p.getImageUrl());
        dto.setDescription(p.getDescription());
        dto.setName(p.getName());
        dto.setPrice(p.getPrice());

        return dto;
    }

    private static String stringOfLength(int n) {
        char[] charArray = new char[n];
        Arrays.fill(charArray, 'a');
        return new String(charArray);
    }
}
