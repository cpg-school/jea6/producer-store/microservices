package nl.producerstore.authentication.service.implementation;

import nl.producerstore.authentication.model.User;
import nl.producerstore.authentication.service.AuthenticationService;
import nl.producerstore.authentication.service.JwtService;
import nl.producerstore.authentication.web.controller.UnauthorizedException;
import nl.producerstore.authentication.web.dto.Credential;
import nl.producerstore.authentication.web.dto.GoogleOauthResponse;
import nl.producerstore.authentication.web.dto.JwtToken;
import nl.producerstore.authentication.web.dto.OauthResponse;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.StringReader;

@ApplicationScoped
public class DefaultAuthenticationService implements AuthenticationService {

    @PersistenceContext(name = "users")
    private EntityManager manager;

    @Inject
    private JwtService jwtService;

    private OkHttpClient httpClient = new OkHttpClient();

    @Override
    public JwtToken authenticate(Credential credential) throws UnauthorizedException {
        User user = manager.createQuery("SELECT u FROM User u WHERE username = :username AND password = :password", User.class)
                .setParameter("username", credential.getUsername())
                .setParameter("password", credential.getPassword())
                .setMaxResults(1)
                .getResultList()
                .stream()
                .findFirst().orElseThrow(UnauthorizedException::new);

        return jwtService.createToken(user.getId(), user.getUsername(), new String[]{"customer"});
    }

    @Override
    public OauthResponse verifyGoogleOauth(String accessToken) {
        Request request = new Request.Builder().url("https://www.googleapis.com/oauth2/v1/userinfo?alt=json&access_token=" + accessToken).build();

        try (Response verifyResponse = httpClient.newCall(request).execute()) {
            JsonObject json = Json.createReader(new StringReader(verifyResponse.body().string())).readObject();

            String username = json.getString("name");
            String id = json.getString("id");

            return GoogleOauthResponse.valid(id, username);

        } catch (Exception e) {
            return GoogleOauthResponse.invalid();
        }
    }
}
