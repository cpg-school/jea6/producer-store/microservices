package nl.producerstore.authentication.service.implementation;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.kumuluz.ee.configuration.utils.ConfigurationUtil;
import nl.producerstore.authentication.service.JwtService;
import nl.producerstore.authentication.web.dto.JwtToken;
import nl.producerstore.authentication.web.dto.OauthResponse;

import javax.enterprise.context.ApplicationScoped;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;

@ApplicationScoped
public class DefaultJwtService implements JwtService {

    private static final String SECRET = ConfigurationUtil.getInstance()
            .get("config.jwt.secret")
            .orElseThrow(() -> new RuntimeException("config.jwt.secret could not be found."));

    private static final String ISSUER = "producerstore.nl";
    private static final String AUDIENCE = "producerstore.nl";
    private static final Algorithm HMAC256 = Algorithm.HMAC256(SECRET);

    @Override
    public JwtToken createToken(long id, String subject, String[] roles) {
        return createToken(Long.toString(id), subject, ISSUER, AUDIENCE, roles);
    }

    @Override
    public JwtToken createTokenForOauth(OauthResponse response, String role) {
        return createToken(response.getUserId(), response.getUsername(), response.getAudience(), ISSUER, role);
    }

    private JwtToken createToken(String subjectId, String subject, String audience, String issuer, String... roles) {
        String token = JWT.create()
                .withSubject(subject)
                .withClaim("id", subjectId)
                .withIssuer(issuer)
                .withAudience(audience)
                .withIssuedAt(new Date())
                .withExpiresAt(toDate(LocalDate.now().plus(Period.ofDays(1))))
                .withArrayClaim("roles", roles)
                .sign(HMAC256);

        return new JwtToken(token);
    }


    private Date toDate(LocalDate dateToConvert) {
        return java.util.Date.from(dateToConvert.atStartOfDay()
                .atZone(ZoneId.systemDefault())
                .toInstant());
    }

}
