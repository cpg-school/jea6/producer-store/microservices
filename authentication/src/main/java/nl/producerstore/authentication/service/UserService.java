package nl.producerstore.authentication.service;

import nl.producerstore.authentication.model.User;

public interface UserService {
    long persist(User user);
}
