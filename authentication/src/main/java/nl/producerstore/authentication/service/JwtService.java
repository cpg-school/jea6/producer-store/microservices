package nl.producerstore.authentication.service;

import nl.producerstore.authentication.web.dto.JwtToken;
import nl.producerstore.authentication.web.dto.OauthResponse;

public interface JwtService {
    JwtToken createToken(long id, String subject, String[] roles);
    JwtToken createTokenForOauth(OauthResponse response, String customer);
}
