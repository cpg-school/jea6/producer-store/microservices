package nl.producerstore.authentication.service;

import nl.producerstore.authentication.web.controller.UnauthorizedException;
import nl.producerstore.authentication.web.dto.Credential;
import nl.producerstore.authentication.web.dto.JwtToken;
import nl.producerstore.authentication.web.dto.OauthResponse;

public interface AuthenticationService {
    JwtToken authenticate(Credential credential) throws UnauthorizedException;
    OauthResponse verifyGoogleOauth(String accessToken);
}
