package nl.producerstore.authentication.service.implementation;

import nl.producerstore.authentication.model.User;
import nl.producerstore.authentication.service.UserService;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

@ApplicationScoped
public class JpaUserService implements UserService {

    @PersistenceContext(name = "users")
    private EntityManager manager;

    @Override
    @Transactional
    public long persist(User user) {
        manager.persist(user);
        return user.getId();
    }
}
