package nl.producerstore.authentication.web.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import nl.producerstore.authentication.service.AuthenticationService;
import nl.producerstore.authentication.service.JwtService;
import nl.producerstore.authentication.web.dto.Credential;
import nl.producerstore.authentication.web.dto.JwtToken;
import nl.producerstore.authentication.web.dto.OauthResponse;

import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/authentication")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class AuthenticationController {

    @Inject
    private AuthenticationService authenticationService;

    @Inject
    private JwtService jwtService;

    @POST
    @Operation(description = "Authenticate a user with a username and password.", tags = {"Authentication"},
            parameters = {
                    @Parameter(name = "Credential", schema = @Schema(implementation = Credential.class))
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "The user is authenticated.", content = @Content(schema = @Schema(implementation = JwtToken.class))),
                    @ApiResponse(responseCode = "401", description = "No user was found with the provided username and password."),
                    @ApiResponse(responseCode = "500", description = "Something went wrong during the authentication process.")
            }
    )
    public Response authenticate(Credential credential) {
        try {
            JwtToken token = authenticationService.authenticate(credential);
            return Response.ok(token).build();

        } catch (UnauthorizedException e) {
            return Response
                    .status(Response.Status.UNAUTHORIZED)
                    .build();

        } catch (Exception e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }

    @POST
    @Path("/oath/google")
    @Operation(description = "Verify an access token provided by Google.", tags = {"Authentication", "Oauth"},
            parameters = {
                    @Parameter(name = "access token", description = "The access token acquired from Google.")
            },
            responses = {
                    @ApiResponse(responseCode = "200", description = "The token was valid, returns a JWT token (with the 'customer' role) to the client.", content = @Content(schema = @Schema(implementation = JwtToken.class))),
                    @ApiResponse(responseCode = "401", description = "The provided access token could not be used to verify the user's identity or was invalid."),
                    @ApiResponse(responseCode = "500", description = "Something went wrong during the validation of the token.")
            }
    )
    @Consumes(MediaType.TEXT_PLAIN)
    public Response authenticateWithGoogleOauth(String accessToken) {
        if (accessToken == null || accessToken.isEmpty()) {
            return Response.status(401).entity("No accessToken provided.").build();
        }

        OauthResponse response = authenticationService.verifyGoogleOauth(accessToken);

        if (!response.isValid()) {
            return Response.status(401).entity("User not authorized").build();
        }

        try {
            JwtToken token = jwtService.createTokenForOauth(response, "customer");
            return Response.ok().entity(token).build();
        } catch (Exception e) {
            return Response.serverError().entity(e.getMessage()).build();
        }
    }
}
