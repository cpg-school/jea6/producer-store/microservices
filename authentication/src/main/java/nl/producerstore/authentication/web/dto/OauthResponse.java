package nl.producerstore.authentication.web.dto;

public interface OauthResponse {
    String getUsername();
    String getUserId();
    boolean isValid();
    String getAudience();
}
