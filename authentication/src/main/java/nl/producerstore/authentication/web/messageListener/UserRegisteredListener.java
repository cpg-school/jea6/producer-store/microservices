package nl.producerstore.authentication.web.messageListener;

import com.kumuluz.ee.amqp.common.annotations.AMQPConsumer;
import nl.producerstore.authentication.model.User;
import nl.producerstore.authentication.service.UserService;
import nl.producerstore.authentication.web.dto.UserDto;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonObject;
import java.io.StringReader;
import java.util.logging.Logger;

@ApplicationScoped
public class UserRegisteredListener implements MessageListener {

    @Inject
    private UserService service;

    private Logger logger = Logger.getLogger(getClass().getName());

    @Override
    @AMQPConsumer(host = "rabbitmq", exchange = "users", key = "user.registered")
    public void onMessage(String message) {
        try {
            JsonObject messageJson = Json.createReader(new StringReader(message)).readObject();
            UserDto dto = UserDto.fromJson(messageJson.getJsonObject("user"));
            User user = dto.toModel();

            long createdId = service.persist(user);

            logger.info("Persisted user with ID " + createdId);
        } catch (Exception e) {
            logger.severe("Could not parse user.\n" + e.getMessage());
        }
    }
}
