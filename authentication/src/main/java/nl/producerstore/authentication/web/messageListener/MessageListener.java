package nl.producerstore.authentication.web.messageListener;

public interface MessageListener {
    void onMessage(String message);
}
