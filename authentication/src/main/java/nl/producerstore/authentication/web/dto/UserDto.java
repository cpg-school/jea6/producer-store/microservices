package nl.producerstore.authentication.web.dto;

import nl.producerstore.authentication.model.User;

import javax.json.JsonObject;

public class UserDto {
    private String username;
    private String password;
    private String email;

    public static UserDto fromJson(JsonObject userJson) {
        UserDto user = new UserDto();

        user.setUsername(userJson.getString("username"));
        user.setPassword(userJson.getString("password"));
        user.setEmail(userJson.getString("email"));

        return user;
    }

    public User toModel() {
        User user = new User();

        user.setEmail(email);
        user.setPassword(password);
        user.setUsername(username);

        return user;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
