package nl.producerstore.authentication.web;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.servers.Server;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

@ApplicationPath("")
@OpenAPIDefinition(info = @Info(
        title = "Producerstore | Authentication API",
        version = "0.0.1"
), servers = {
        @Server(
                url = "http://localhost:8004",
                description = "Default server")
})
public class AuthenticationApi extends Application {
}
