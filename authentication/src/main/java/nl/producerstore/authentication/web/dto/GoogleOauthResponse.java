package nl.producerstore.authentication.web.dto;

public class GoogleOauthResponse implements OauthResponse {
    private String userId;
    private String username;
    private boolean isValid;

    public static OauthResponse valid(String username, String id) {
        return new GoogleOauthResponse(username, id, true);
    }

    public static OauthResponse invalid() {
        return new GoogleOauthResponse(null, null, false);
    }

    private GoogleOauthResponse(String userId, String username, boolean isValid) {
        this.userId = userId;
        this.username = username;
        this.isValid = isValid;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public boolean isValid() {
        return isValid;
    }

    @Override
    public String getAudience() {
        return "oauth/v2/google";
    }

    public void setValid(boolean valid) {
        isValid = valid;
    }
}
