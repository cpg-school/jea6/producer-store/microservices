"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const http_1 = __importDefault(require("http"));
const express_1 = __importDefault(require("express"));
const utils_1 = require("./utils");
const OrderController_1 = __importDefault(require("./controllers/OrderController"));
const extract_user_1 = __importDefault(require("./middleware/extract-user"));
const bodyParser = require("body-parser");
const MongoOrderService_1 = __importDefault(require("./services/MongoOrderService"));
const router = express_1.default();
const { PORT = 3000 } = process.env;
const server = http_1.default.createServer(router);
router.use(extract_user_1.default);
router.use(bodyParser.json());
const connectionUrl = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT || '27017'}`;
const collectionName = "orders";
utils_1.getMongoCollection(connectionUrl, collectionName)
    .then((orderCollection) => { utils_1.applyRoutes(OrderController_1.default(new MongoOrderService_1.default(orderCollection)), router); });
server.listen(PORT, () => console.log(`Server is running at port ${PORT}`));
//# sourceMappingURL=server.js.map