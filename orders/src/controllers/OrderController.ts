import { Request, Response } from "express";
import { Route } from "../utils";
import { NextFunction } from "connect";
import OrderSerivce from "../services/OrderService";
import OrderLine from "../models/OrderLine";
import url from 'url';

export default (service: OrderSerivce): Route[] =>
    [
        {
            path: "/users/me/orders",
            method: "get",
            handler: async (req: Request, res: Response, next: NextFunction): Promise<void> => {
                const userId = parseInt(req.headers['x-user-id'] as string);
                
                const orders = await service.getOrdersForUser(userId);

                if (!orders || orders.length == 0) {
                    res.status(204).send("No orders found for user with id: " + userId);
                    return;
                }

                res.status(200).json(orders);
            }
        },
        {
            path: "/users/me/orders/:id",
            method: "get",
            handler: async (req: Request, res: Response, next: NextFunction): Promise<void> => {
                const userId = parseInt(req.headers['x-user-id'] as string);
                const orderId = parseInt(req.params.id);

                const order = await service.getById(orderId);

                if (!order || order.userId != userId) {
                    res.status(404).send(`Could not find an order with id "${orderId}" for user with id "${userId}`);
                    return;
                }

                res.status(200).json(order);
            }
        },
        {
            path: "/users/me/orders",
            method: "post",
            handler: async (req: Request, res: Response, next: NextFunction): Promise<void> => {
                const userId = parseInt(req.headers['x-user-id'] as string);
                const orderLines: OrderLine[] = req.body;

                const id = await service.addOrderForUser(userId, orderLines);

                const baseUrl = url.format({
                    protocol: req.protocol,
                    host: req.get('host'),
                    pathname: req.originalUrl
                })

                res.status(201).location(`${baseUrl}/${id}`).send();
            }
        }
    ]


