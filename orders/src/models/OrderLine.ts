import Product from "./Product";

export default interface OrderLine {
    product: Product;
    amount: number;
}
