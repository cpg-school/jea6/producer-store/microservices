import OrderLine from "./OrderLine";

export default interface Order {
    id: number,
    userId: number;
    orderLines: OrderLine[]
    date: Date
}

