import http, { Server } from 'http'
import express, { Application } from 'express'
import { applyRoutes, getMongoCollection } from './utils';
import orderControllerRoutes from './controllers/OrderController';
import extractUser from './middleware/extract-user';
import bodyParser = require('body-parser');
import MongoOrderService from './services/MongoOrderService';
import { MongoClient, Collection } from 'mongodb';
import Order from './models/Order';

const router: Application = express();
const { PORT = 3000 } = process.env;
const server: Server = http.createServer(router);

router.use(extractUser)
router.use(bodyParser.json())


const connectionUrl = `mongodb://${process.env.MONGO_USER}:${process.env.MONGO_PASS}@${process.env.MONGO_HOST}:${process.env.MONGO_PORT || '27017'}`
const collectionName = "orders";

getMongoCollection(connectionUrl, collectionName)
    .then((orderCollection: Collection<Order>) => { applyRoutes(orderControllerRoutes(new MongoOrderService(orderCollection)), router);})


server.listen(PORT, () => console.log(`Server is running at port ${PORT}`))
