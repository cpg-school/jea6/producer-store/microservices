import Order from "../models/Order";
import OrderLine from "../models/OrderLine";
export default interface OrderService {
    getById(orderId: number): Promise<Order | null> | Order | null;
    getOrdersForUser(userId: number): Promise<Order[]> | Order[];
    addOrderForUser(userId: number, orderLines: OrderLine[]): Promise<number> | number;
}
