import OrderService from "./OrderService";
import OrderLine from "../models/OrderLine";
import Order from "../models/Order";
import { MongoClient, Db, Collection } from "mongodb";



export default class MongoOrderService implements OrderService {

    constructor(private orders: Collection<Order>) { }

    async getById(orderId: number): Promise<Order | null> {
        return await this.orders.findOne({id: orderId})
    }

    async getOrdersForUser(userId: number): Promise<Order[]> {
        return await this.orders.find({userId: userId}).toArray();
    }

    async addOrderForUser(userId: number, orderLines: OrderLine[]): Promise<number> {
        const id: number = await this.orders.count() + 1;
        
        await this.orders.insertOne({
            id,
            date: new Date(),
            orderLines: orderLines,
            userId: userId
        });

        return id;
    }
}