import { NextFunction, Router, Request, Response } from "express";
import { MongoClient, Collection } from "mongodb";
import Order from "../models/Order";

export type Route = {
  path: string;
  method: string;
  handler: (req: Request, res: Response, next: NextFunction) => Promise<void> | void
};

export const applyRoutes = (routes: Route[], router: Router) => {
  for (const route of routes) {
    const { method, path, handler } = route;
    (router as any)[method](path, handler);
  }
};


export const getMongoCollection = (connectionUrl: string, collectionName: string): Promise<Collection<Order>> => {
  return new Promise((resolve, reject) => {
    console.log(`Connecting to mongo with: ${connectionUrl}`)

    MongoClient.connect(connectionUrl, { useNewUrlParser: true }, (error, client) => {
      if (error) {
        reject(error.message);
      } else {
        resolve(client.db('admin').collection(collectionName))
      }
    });
  })
}