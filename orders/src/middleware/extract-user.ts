import { Request, Response } from "express";
import { NextFunction } from "connect";
var atob = require('atob')

const extractUser = (req: Request, res: Response, next: NextFunction) => {
    const authHeader = req.headers.authorization as string;

    if (!authHeader) {
        unauthorized(res, "No authorization header present.")
    }

    try {
        const token = getToken(authHeader);
        const decoded = decodeToken(token);

        if (!decoded.id) {
            console.log('?/?')
            throw new Error("No userId was present in provided JWT.")
        }
        
        req.headers['x-user-id'] = decoded.id;

    } catch (e) {
        unauthorized(res, e.message)
    }


    next();
}

const unauthorized = (res: Response, message: string) => {
    res.status(401).send(message);
}

const getToken = (authHeader: string): string => {
    const parts = authHeader.split("Bearer ")

    if (parts.length != 2) {
        throw new Error("No JWT provided.")
    }

    return parts[1];
}

const decodeToken = (token: string) => {
    const base64Url = token.split('.')[1];
    const base64 = decodeURIComponent(atob(base64Url).split('').map((c: string) => '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2)).join(''));

    return JSON.parse(base64);
}

export default extractUser;