import argparse
import subprocess
import glob

def find_pom(project_dir):
    for file in glob.glob(project_dir + '/pom.xml'):
        return file


def main():
    argparser = argparse.ArgumentParser()
    argparser.add_argument(
        '-s', '--service', help='The name of the serivce in the docker-compose file.')
    argparser.add_argument(
        '-p', '--project', help='The project directory where the pom.xml file resides')

    args = argparser.parse_args()

    service_name = args.service
    project_name = service_name if args.project is None else args.project

    pom_file = find_pom(project_name)

    #subprocess.run(['docker-compose', 'stop', service_name, '&&', 'mvn', '-f', pom_file, 'clean', 'package', '&&', 'docker-compose', 'start', service_name])
    subprocess.Popen(f'docker-compose stop {service_name} && mvn -f {pom_file} clean package && docker-compose start {service_name}', shell=True)


if __name__ == "__main__":
    main()
