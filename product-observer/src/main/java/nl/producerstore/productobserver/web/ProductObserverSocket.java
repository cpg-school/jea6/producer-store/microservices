package nl.producerstore.productobserver.web;

import nl.producerstore.productobserver.service.ProductObserverService;

import javax.inject.Inject;
import javax.websocket.OnClose;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

@ServerEndpoint("/observe/{productId}/{username}")
public class ProductObserverSocket {

    @Inject
    private ProductObserverService service;

    @OnMessage
    public void onMessage(String message, Session session) {
        String productId = session.getPathParameters().get("productId");
        String username = session.getPathParameters().get("username");

        if (productId == null || username == null) {
            return;
        }

        service.sendMessage(username, productId, message);
    }

    @OnOpen
    public void onOpen(Session session) {
        String productId = session.getPathParameters().get("productId");

        if (productId == null) {
            return;
        }

        service.addWatcher(productId, session);
        service.informChange(productId);
    }

    @OnClose
    public void onClose(Session session) {
        String productId = session.getPathParameters().get("productId");

        if (productId == null) {
            return;
        }

        service.removeWatcher(productId, session);
        service.informChange(productId);
    }
}
