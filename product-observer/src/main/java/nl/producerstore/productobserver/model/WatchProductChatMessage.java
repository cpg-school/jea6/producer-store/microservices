package nl.producerstore.productobserver.model;

public class WatchProductChatMessage extends WebsocketMessage {

    private String productId;
    private String from;
    private String message;

    public WatchProductChatMessage(String from, String productId, String message) {
        super("chat-message");
        this.productId = productId;
        this.from = from;
        this.message = message;
    }

    public String getProductId() {
        return productId;
    }

    public String getFrom() {
        return from;
    }

    public String getMessage() {
        return message;
    }
}
