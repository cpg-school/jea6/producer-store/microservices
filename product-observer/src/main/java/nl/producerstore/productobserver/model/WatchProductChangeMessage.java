package nl.producerstore.productobserver.model;

public class WatchProductChangeMessage extends WebsocketMessage {

    private String productId;
    private int watchers;

    public WatchProductChangeMessage(String productId, int watchers) {
        super("watchers-change");
        this.productId = productId;
        this.watchers = watchers;
    }

    public String getProductId() {
        return productId;
    }

    public int getWatchers() {
        return watchers;
    }
}
