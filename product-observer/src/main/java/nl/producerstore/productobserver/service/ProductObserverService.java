package nl.producerstore.productobserver.service;

import javax.websocket.Session;
import java.util.List;

public interface ProductObserverService {
    /**
     * Adds a watcher to the given product.
     * @param productId id of the product being watched.
     * @param session websocket session of the user.
     */
    void addWatcher(String productId, Session session);

    /**
     * Removes a watcher from the given product.
     * @param productId id of the product being watched.
     * @param session websocket session of the user.
     */
    void removeWatcher(String productId, Session session);

    /**
     * Sends an update to all subscribed sessions for the given product.
     * @param productId id of the product.
     */
    void informChange(String productId);

    /**
     * Send a message to all subscribers in the product channel.
     * @param fromUser name of the user who sent the message.
     * @param productId id of the target product.
     * @param message content of the message.
     */
    void sendMessage(String fromUser, String productId, String message);

    /**
     * Get a list of all current sessions for a given product.
     * @param productId the id of the product.
     * @return a list of all current sessions for the specified product.
     */
    List<Session> getWatchersForSession(String productId);
}
