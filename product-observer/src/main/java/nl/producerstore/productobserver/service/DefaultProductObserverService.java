package nl.producerstore.productobserver.service;

import com.google.gson.Gson;
import nl.producerstore.productobserver.model.WatchProductChangeMessage;
import nl.producerstore.productobserver.model.WatchProductChatMessage;
import nl.producerstore.productobserver.model.WebsocketMessage;

import javax.websocket.EncodeException;
import javax.websocket.Session;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DefaultProductObserverService implements ProductObserverService {
    private static Map<String, List<Session>> watchersForSession = new HashMap<>();
    private Gson jsonConverter = new Gson();

    @Override
    public void addWatcher(String productId, Session session) {
        List<Session> watchers = getWatchersForSession(productId);

        watchers.add(session);

        watchersForSession.put(productId, watchers);
    }

    @Override
    public void removeWatcher(String productId, Session session) {
        List<Session> watchers = getWatchersForSession(productId);

        watchers.remove(session);

        watchersForSession.put(productId, watchers);
    }

    @Override
    public void informChange(String productId) {
        List<Session> watchers = getWatchersForSession(productId);
        sendMessage(watchers, new WatchProductChangeMessage(productId, watchers.size()));
    }

    @Override
    public void sendMessage(String fromUser, String productId, String message) {
        sendMessage(getWatchersForSession(productId), new WatchProductChatMessage(fromUser, productId, message));
    }

    //region helper methods
    private void sendMessage(List<Session> watchers, WebsocketMessage message) {
        for (Session session : watchers) {
            try {
                session.getBasicRemote()
                        .sendObject(jsonConverter.toJson(message));

            } catch (IOException | EncodeException e) {
                e.printStackTrace();
            }
        }
    }

    public List<Session> getWatchersForSession(String productId) {
        return watchersForSession.containsKey(productId)
                ? watchersForSession.get(productId)
                : new ArrayList<>();
    }
    //endregion
}
