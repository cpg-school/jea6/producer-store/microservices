package nl.producerstore.userservice.web.messaging;

import nl.producerstore.userservice.model.User;

public interface MessageProducer {

    void userRegistered(User user);

}
