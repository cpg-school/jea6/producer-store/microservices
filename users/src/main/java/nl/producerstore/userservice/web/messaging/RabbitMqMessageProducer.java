package nl.producerstore.userservice.web.messaging;

import com.kumuluz.ee.amqp.common.annotations.AMQPChannel;
import com.kumuluz.ee.configuration.utils.ConfigurationUtil;
import com.rabbitmq.client.Channel;
import nl.producerstore.userservice.model.User;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.json.Json;
import java.io.IOException;


@ApplicationScoped
public class RabbitMqMessageProducer implements MessageProducer {

    @Inject
    @AMQPChannel("rabbitmq")
    private Channel channel;

    private String usersExchange = ConfigurationUtil.getInstance()
            .get("config.rabbitmq.usersExchange")
            .orElseThrow(() -> new RuntimeException("config.rabbitmq.usersExchange could not be found."));

    @Override
    public void userRegistered(User user) {
        String message = Json.createObjectBuilder()
                .add("user", Json.createObjectBuilder()
                        .add("username", user.getUsername())
                        .add("password", user.getPassword())
                        .add("email", user.getEmail()))
                .build()
                .toString();

        try {
            channel.basicPublish(usersExchange, "user.registered", null, message.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
