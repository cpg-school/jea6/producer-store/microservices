package nl.producerstore.userservice.web.controller;


import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.headers.Header;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import nl.producerstore.userservice.model.User;
import nl.producerstore.userservice.web.messaging.MessageProducer;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.util.Set;

@Path("/users")
@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class UserController {

    @Inject
    private Validator validator;

    @Inject
    private MessageProducer messageProducer;

    @POST
    @Operation(description = "Create a new user.", tags = {"Users"}, responses = {
            @ApiResponse(responseCode = "201", description = "User successfully created.", headers = {@Header(name = "location", description = "The location where the user was created at.")}),
            @ApiResponse(responseCode = "400", description = "Another user with the same email or username already exists."),
            @ApiResponse(responseCode = "500", description = "Something unexpected went wrong.")
    }, parameters = {
            @Parameter(name = "user", description = "The user that you want to create.", schema = @Schema(implementation = User.class, name = "user"))
    })
    public Response createUser(User user, @Context UriInfo uriInfo) {
        Set<ConstraintViolation<User>> errors = validator.validate(user);

        if (!errors.isEmpty()) {
            return validationError(errors);
        }

        try {
            messageProducer.userRegistered(user);
            return Response.noContent().build();
        } catch (Exception e) {
            return Response.serverError()
                    .type(MediaType.TEXT_PLAIN_TYPE)
                    .entity(e.getMessage())
                    .build();
        }
    }

    private Response validationError(Set<ConstraintViolation<User>> violations) {
        JsonArrayBuilder errors = Json.createArrayBuilder();

        for (ConstraintViolation<User> violation : violations) {
            errors.add(Json.createObjectBuilder()
                    .add("parameter", violation.getPropertyPath().toString())
                    .add("error", violation.getMessage())
            );
        }
        return Response.status(400)
                .type(MediaType.APPLICATION_JSON)
                .entity(Json.createObjectBuilder()
                        .add("errors", Json.createObjectBuilder()
                                .add("validationErrors", errors)
                                .build())
                        .build()
                        .toString())
                .build();
    }
}
