package nl.producerstore.userservice.producer;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Produces;
import javax.validation.Validation;
import javax.validation.Validator;

@ApplicationScoped
public class ValidatorProducer {

    @Produces
    public Validator createValidator() {
        return Validation.buildDefaultValidatorFactory().getValidator();
    }
}
